;Common Noffz constants accross all builds in this project

;The root folder for output build
#define outputDir "C:\work\nprorekhin-cla-summit-2019\builds\installers\
;#define outputDir "C:\Users\user\Desktop\temp"

;The root of the project folder. Will be used in [files] section
;#define projectFolder "C:\work\utp-logger"
;It is defined in during compile time

#define wizardSmallImageFile (projectFolder + "\installers\graphics\noffz-wizard-small.bmp")
#define wizardImageFile (projectFolder + "\installers\graphics\noffz-wizard.bmp")
#define pathToFonts (projectFolder + "\externals\graphics\fonts")

#define customErrorNIDestFolder "{pf}\National Instruments\Shared\Errors"

#define packageVersion "1.0.0"
;Define all product version also here and make them the same version
#define version3 "1.0.0"
;Leave prerelease blank if it is a release. put "-alpha", "-beta" or any other postfix if there is work is a progress
#define prerelease ""

;Text name for the package
#define packageVersionTextName ("CLA Edition" + prerelease)

;Calculated variables
;#lvPath, #lvName, #lvYear
#if lvXX=="lv14" 
  #define lvYear="2014"
#elif lvXX=="lv15" 
  #define lvYear="2015"
#elif lvXX=="lv16" 
  #define lvYear="2016"
#elif lvXX=="lv17" 
  #define lvYear="2017"
#elif lvXX=="lv18" 
  #define lvYear="2018"
#elif lvXX=="lvNO"
  #define lvYear=""
#else
  #error Unsupported lvXX! Use lvNO,lv14, lv15, lv16, lv17 or lv18. If you want to use another version of LabVIEW, modify the script 
#endif
  #define lvName="LabVIEW "+lvYear
  #define lvPath="{pf}\National Instruments\"+lvName


;Define possible TS path for installing Plugin API
;Define TestStand directories
#define ts2010SP1Folder="C:\Program Files (x86)\National Instruments\TestStand 2010 SP1"
#define ts2012Folder="C:\Program Files (x86)\National Instruments\TestStand 2012"
#define ts2013Folder="C:\Program Files (x86)\National Instruments\TestStand 2013"
#define ts2014x86Folder="C:\Program Files (x86)\National Instruments\TestStand 2014"
#define ts2014x64Folder="C:\Program Files\National Instruments\TestStand 2014"
#define ts2016x86Folder="C:\Program Files (x86)\National Instruments\TestStand 2016"
#define ts2016x64Folder="C:\Program Files\National Instruments\TestStand 2016"
#define ts2017x86Folder="C:\Program Files (x86)\National Instruments\TestStand 2017"
#define ts2017x64Folder="C:\Program Files\National Instruments\TestStand 2017"

;Define TestStand names
#define tsName2010SP1 "TestStand 2010 SP1"
#define tsName2012 "TestStand 2012"
#define tsName2013 "TestStand 2013"
#define tsName2014x86 "TestStand 2014 (32-bit)"
#define tsName2014x64 "TestStand 2014 (64-bit)"
#define tsName2016x86 "TestStand 2016 (32-bit)"
#define tsName2016x64 "TestStand 2016 (64-bit)"
#define tsName2017x86 "TestStand 2017 (32-bit)"
#define tsName2017x64 "TestStand 2017 (64-bit)"

;Define TestStand public components directories
#define ts2010SP1PublicFolder="{commondocs}\National Instruments\" + tsName2010SP1
#define ts2012PublicFolder="{commondocs}\National Instruments\" + tsName2012
#define ts2013PublicFolder="{commondocs}\National Instruments\" + tsName2013
#define ts2014x86PublicFolder="{commondocs}\National Instruments\" + tsName2014x86
#define ts2014x64PublicFolder="{commondocs}\National Instruments\" + tsName2014x64
#define ts2016x86PublicFolder="{commondocs}\National Instruments\" + tsName2016x86
#define ts2016x64PublicFolder="{commondocs}\National Instruments\" + tsName2016x64
#define ts2017x86PublicFolder="{commondocs}\National Instruments\" + tsName2017x86
#define ts2017x64PublicFolder="{commondocs}\National Instruments\" + tsName2017x64

;#tsPath,#tsName,#tsYear
#if tsXX=="ts10sp1" 
  #define tsYear="2010 SP1"
  #define tsPublicPath=ts2010SP1PublicFolder
#elif tsXX=="ts12" 
  #define tsYear="2012"
  #define tsPublicPath=ts2012PublicFolder
#elif tsXX=="ts14" 
  #define tsYear="2014"
  #if xXX=="x64"
    #define tsPublicPath=ts2014x86PublicFolder
  #elif xXX=="x86"
    #define tsPublicPath=ts2014x86PublicFolder
  #else
    #error Unsupported xXX! Use x64 or x86.
  #endif
#elif tsXX=="ts16" 
  #define tsYear="2016"
  #if xXX=="x64"
    #define tsPublicPath=ts2016x64PublicFolder
  #elif xXX=="x86"
    #define tsPublicPath=ts2016x86PublicFolder
  #else
    #error Unsupported xXX! Use x64 or x86.
  #endif
#elif tsXX=="ts17" 
  #define tsYear="2017"
  #if xXX=="x64"
    #define tsPublicPath=ts2017x64PublicFolder
  #elif xXX=="x86"
    #define tsPublicPath=ts2017x86PublicFolder
  #else
    #error Unsupported xXX! Use x64 or x86.
  #endif
#elif tsXX=="tsNO" 
  #define tsYear=""
#else
  #error Unsupported tsXX! Use ts10sp1, ts12, ts14, s16 or tsNO. If you want to use another version of TestStand, modify the script 
#endif
#define tsName="TestStand "+tsYear
#define tsPath="{pf}\National Instruments\"+tsName

;Define compile time (not run-time!) Path to LabVIEW and store it to {#lvPathCompileTime}
#if xXX=="x64"
  #define pfCompileTime="C:\Program Files"
#elif xXX=="x86"
  #define pfCompileTime="C:\Program Files (x86)"
#else
  #error Unsupported xXX! Use x64 or x86.
#endif
#define lvPathCompileTime=pfCompileTime+"\National Instruments\"+lvName

