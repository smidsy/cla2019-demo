[Dirs]

[Files]
Source: "installer.ico"; DestDir: "{commonappdata}\{#uninstFolder}\"; Flags: ignoreversion

Source: "{#projectFolder}\builds\app\*"; DestDir: "{app}"; Flags: ignoreversion

[Icons]
Name: "{userdesktop}\{#productName}"; Filename: "{app}\CLA Application.exe"

[UninstallRun]
Filename: "taskkill"; Parameters: "/im ""CLA Application.exe"" /f"; Flags: runhidden

;Enable this expression if you want to see the iss file after all preprocessing steps
;#expr SaveToFile(AddBackslash(SourcePath) + "preprocessed.iss")


