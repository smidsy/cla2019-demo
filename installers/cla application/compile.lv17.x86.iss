;Version specific constants
#define appId "{B493727D-192A-4E96-B0A1-DD4F64E3332A}"
#define lvXX "lv17"
#define tsXX "tsNO"
#define xXX "x86"
#define OSbitXX "x86"

;Include all files
#include "..\common.header.iss"
#include "constants.iss"
#include "..\common.setup.iss"
#include "installer.iss"
#include "..\common.uninstall.iss"