;Common project constants
#define productName "CLA Application From Installer"
#define installerFileName "Noffz.CLA.Summit.CLA Application"

;Standard setting for installers in \dp
#define dp "\dp"

;Expected string Noffz.UTP.IO.NI DAQmx.plugin.lv15.x86
#define outputBaseFilename installerFileName

;Expected string: UTP IO Core for LabVIEW 2014 (x86) - 1.0.0.4
#define appName (productName+ " for " +lvName+ " (" +xXX+ ") - "+ version3 + "." + revision)


;Defines the main ({app})destination folder
#define defaultDirName "{pf}\Noffz\CLA 2019 App From Installer\"

;Defines relative path starting from {commonappdata} for storing uninst files
#define uninstFolder ("Noffz\CLA\uninst\{"+ AppId)



