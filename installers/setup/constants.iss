;Common project constants
#define productName "UTP IO Package"
#define installerFileName "setup"

#define dp ""

;Expected string Noffz.UTP.Logger.LabVIEW.lv14.x86.1.2.3.4
#define outputBaseFilename installerFileName

;Expected string: UTP Logger for LabVIEW 2014 (x86) - 1.0.0.4
#define appName (productName + " " + packageVersion)

;{app} constant will be refering to defaultDirName in silent mode
#define defaultDirName ""

#define uninstFolder "Noffz\UTP IO Package\uninst"
#define InstallSetupINI ("ExpandConstant('{src}')+'\dp\"+ installerFileName + ".ini'")
#define UninstallSetupINI ("ExpandConstant('{commonappdata}')+'\" + uninstFolder + "\" + installerFileName + ".ini'")






