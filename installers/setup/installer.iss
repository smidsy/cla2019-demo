DisableWelcomePage=yes
DisableDirPage=yes
DisableReadyPage=yes

CreateAppDir=False

#expr CopyFile("setup.ini", (outputDir+"\"+version3+prerelease+"\dp\setup.ini") )
#expr CopyFile("..\uninstall previous\uninstall.bat", (outputDir+"\"+version3+prerelease+"\dp\uninstall.bat") )

[Files]
Source: "installer.ico"; DestDir: "{commonappdata}\{#uninstFolder}\"; Flags: ignoreversion
Source: "setup.ini"; DestDir: "{commonappdata}\{#uninstFolder}\"; Flags: ignoreversion

[CustomMessages]
FinishedLabelRestartWarning=To complete the installation of LabVIEW Run-Time Engine, you must restart your computer after installing is completed!

[Code]
const
  CRLF = #13#10;
  SettingsPageID = 100;
  ReadyPageID = 101;
  InstallationProgressPageID = 102;
   
var
  ComponentsCheckListBox: TNewCheckListBox;
  ReadyPage: TOutputMsgMemoWizardPage;
  InstallationProgressPage: TOutputProgressWizardPage;
  ComponentNumber: Integer;
  NoShortcutsCheckBox: TNewCheckBox;
  IDDictionary: TStringList;
  
function StrToBool(Text: String): Boolean;
begin
 if Uppercase(Text)='TRUE' then Result := True
 else Result := False;
end;

function StrSplit(Text: String; Separator: String): TArrayOfString;
var
  i, p: Integer;
  Dest: TArrayOfString; 
begin
  i := 0;
  repeat
    SetArrayLength(Dest, i+1);
    p := Pos(Separator,Text);
    if p > 0 then begin
      Dest[i] := Copy(Text, 1, p-1);
      Text := Trim(Copy(Text, p + Length(Separator), Length(Text)));
      i := i + 1;
    end else begin
      Dest[i] := Trim(Text);
      Text := '';
    end;
  until Length(Text)=0;
  Result := Dest
end;

function GetComponentParams(ACheckListBox: TNewCheckListBox; index: Integer): String;
begin
  Result := Trim(TStrings(ACheckListBox.ItemObject[index]).Strings[4])
end;

function GetComponentShowCmd(ACheckListBox: TNewCheckListBox; index: Integer): Integer;
begin
  Result := StrToInt(TStrings(ACheckListBox.ItemObject[index]).Strings[5])
end;

function GetCheckedComponentFullPath(ACheckListBox: TNewCheckListBox; index: Integer): String;
var
   ARelativePath: String;
begin
    Result := '';
    ARelativePath := Trim(TStrings(ACheckListBox.ItemObject[index]).Strings[0]);
    if (ARelativePath <> '') and (ACheckListBox.Checked[index]) then 
       Result := ExpandConstant('{src}\') + ARelativePath;
     
  
end;

function GetComponentFullName(ACheckListBox: TNewCheckListBox; index: Integer): String;
begin
  case ACheckListBox.ItemLevel[index] of
  1: if GetCheckedComponentFullPath(ACheckListBox, index) <>'' then Result := ACheckListBox.ItemCaption[index];
  2: Result := TStrings(ACheckListBox.ItemObject[index]).Strings[1] + ' ' + ACheckListBox.ItemCaption[index];
  else Result := '';
   
  end;
end;

function GetNumCheckedComponents(ACheckListBox: TNewCheckListBox): Integer;
var
   index: Integer;
begin
   Result := 0;
   for index := 0 to (ComponentNumber - 1) do
   begin
      if GetCheckedComponentFullPath(ACheckListBox, index) <> '' then Result := Result + 1;
   end;
end;

function GetCheckedComponentPaths(ACheckListBox: TNewCheckListBox): TStringList;
var
    index: Integer;
    path: String;
begin
    Result := TStringList.Create;
    for index := 0 to (ComponentNumber - 1) do
    begin  
       path := GetCheckedComponentFullPath(ACheckListBox, index);
      if path <> '' then Result.Append(path);
    end;
end;

function GetCheckedComponentNames(ACheckListBox: TNewCheckListBox): TStringList;
var
    index: Integer;
begin
    Result := TStringList.Create;
    for index := 0 to (ComponentNumber - 1) do                                                                               
      if GetCheckedComponentFullPath(ACheckListBox, index) <> '' then Result.Append(GetComponentFullName(ACheckListBox, index));
end;

function GetCheckedComponentIndexes(ACheckListBox: TNewCheckListBox): TStringList;
var
    index: Integer;
begin
    Result := TStringList.Create;
    for index := 0 to (ComponentNumber - 1) do                                                                               
      if GetCheckedComponentFullPath(ACheckListBox, index) <> '' then Result.Append(IntToStr(index));
end;

procedure URLLabelOnClick(Sender: TObject);
var
  ErrorCode: Integer;
begin
  ShellExecAsOriginalUser('open', 'http://www.noffz.com/', '', '', SW_SHOWNORMAL, ewNoWait, ErrorCode);
end;

procedure CreateURLLabel(ParentForm: TSetupForm);
var
  URLLabel: TNewStaticText;
begin
  URLLabel := TNewStaticText.Create(ParentForm);
  URLLabel.Caption := 'www.noffz.com';
  URLLabel.Cursor := crHand;
  URLLabel.OnClick := @URLLabelOnClick;
  URLLabel.Parent := ParentForm;
  { Alter Font *after* setting Parent so the correct defaults are inherited first }
  URLLabel.Font.Style := URLLabel.Font.Style + [fsUnderline];
  if GetWindowsVersion >= $040A0000 then   { Windows 98 or later? }
    URLLabel.Font.Color := clHotLight
  else
    URLLabel.Font.Color := clBlue;
  URLLabel.Top := ParentForm.ClientHeight - ScaleY(23 + 10);
  URLLabel.Left := ScaleX(20);

end;

procedure ComponentsCheckListBoxOnClick(Sender: TObject);
var
  i: Integer;
  ItemIndex: Integer;
  ItemDependencies: TArrayOfString;
  CheckAction: TCheckItemOperation;
  ItemChecked: Boolean;
  ItemObjectIndex: Integer;
  ItemRealIndex: Integer;

  begin
  ItemIndex := TNewCheckListBox(Sender).AbsItemIndex;
  ItemChecked := TNewCheckListBox(Sender).Checked[ItemIndex];
  try
    if ItemChecked then
    begin
      CheckAction := coCheckWithChildren;
      ItemObjectIndex := 2;
    end
    else begin
      CheckAction := coUncheck;
      ItemObjectIndex := 3;
    end;
    ItemDependencies := StrSplit(TStrings(TNewCheckListBox(Sender).ItemObject[ItemIndex]).Strings[ItemObjectIndex], ',');
    for i:=0 to (GetArrayLength(ItemDependencies) - 1) do 
    begin
      if ItemDependencies[i] <> '' then
      begin
        ItemRealIndex := IDDictionary.IndexOf(ItemDependencies[i]);
        if (ItemRealIndex >= 0) then TNewCheckListBox(Sender).CheckItem(ItemRealIndex, CheckAction);
      end
    end;
    
  except
//      MsgBox('.', mbInformation, mb_Ok);
  finally
  end;
end;


procedure CreateSettingsPage;
var
  SettingsPage: TWizardPage;
  SettingsDescription: TNewStaticText;
  
  ConfigStrings: TArrayOfString;
  ConfigStringValues: TArrayOfString;
  
  index: integer;
  
  ComponentCaption: String;
  ComponentType: Byte; //0 -- Checkbox, 1 -- Expanded Checkbox, 2 -- Group,  3 -- Expanded Group
  IsGroup: Boolean;
  IsExpanded: Boolean;
  ComponentLevel: Byte;
  ComponentChecked: Boolean;
  ComponentEnabled: Boolean;
  Require64bit: Boolean;
  BitnessOK: Boolean;
  DependenciesOK: Boolean;
  ComponentVisible: Boolean;
  

begin
  IDDictionary := TStringList.Create;
  SettingsPage := CreateCustomPage(wpWelcome, 'Select Components', 'Which components should be installed?');
  SettingsDescription := TNewStaticText.Create(SettingsPage);

  SettingsDescription.Caption := 'Select the components you want to install; clear the components you do not want to install. Click Next when you are ready to continue.';
  SettingsDescription.Autosize := True;
  SettingsDescription.WordWrap := True;
  SettingsDescription.Parent := SettingsPage.Surface;

  ComponentsCheckListBox := TNewCheckListBox.Create(SettingsPage);

  ComponentNumber := 0;

  try
  with ComponentsCheckListBox do
  begin
    Width := SettingsPage.SurfaceWidth;
    Height := ScaleY(209);
    Flat := False;
    Parent := SettingsPage.Surface;
    LoadStringsFromFile({#InstallSetupINI}, ConfigStrings);
    TreeViewStyle := True;
	
    for index := 0 to (GetArrayLength(ConfigStrings)-1) do
    begin
      OnClickCheck := @ComponentsCheckListBoxOnClick; //do not use OnClick
      ConfigStringValues := StrSplit(ConfigStrings[index], ';');
      ComponentCaption := ConfigStringValues[0];
      ComponentType := StrToInt(ConfigStringValues[1]);
      IsGroup := ((ComponentType div 2)>0);
	    IsExpanded := ((ComponentType mod 2)>0);
	    ComponentLevel := StrToInt(ConfigStringValues[2]);
      ComponentChecked := StrToBool(ConfigStringValues[3]);
      Require64bit := StrToBool(ConfigStringValues[5]);
      
      if Require64bit then BitnessOK := IsWin64
      else BitnessOK := True;
	  
	    if ConfigStringValues[13]<>'' then
	    begin
	      if Require64bit then
		  begin
		  	if IsWin64 then 
		  	  begin
            DependenciesOK := FileExists(ExpandConstant(('{pf64}\')) + ConfigStringValues[13]);
	        end
			      else DependenciesOK := False;
		      end
        else DependenciesOK := FileExists(ExpandConstant(('{pf32}\')) + ConfigStringValues[13]);
	    end
	    else DependenciesOK := True;

      ComponentVisible := BitnessOK and DependenciesOK;

      if IsGroup then AddGroupEx(ComponentCaption, '', ComponentLevel, TStringList.Create, IsExpanded) //the shit won't work with TStrings; actually, do not use TStrings at all.
      else if ComponentVisible then
      begin     
        ComponentEnabled := StrToBool(ConfigStringValues[4]);
        AddCheckBoxEx(ComponentCaption, '', ComponentLevel, ComponentChecked, ComponentEnabled, True, True, TStringList.Create, IsExpanded);
      end;
    
      if (ComponentVisible or IsGroup) then
      begin
        TStringList(ItemObject[ComponentNumber]).Append(ConfigStringValues[6]);  //0 relative path    
        TStringList(ItemObject[ComponentNumber]).Append(ConfigStringValues[7]);  //1 base (parent) name
        TStringList(ItemObject[ComponentNumber]).Append(ConfigStringValues[8]);  //2 dependency check list
        TStringList(ItemObject[ComponentNumber]).Append(ConfigStringValues[9]);  //3 dependency uncheck list
        TStringList(ItemObject[ComponentNumber]).Append(ConfigStringValues[10]); //4 Parameters
        TStringList(ItemObject[ComponentNumber]).Append(ConfigStringValues[11]); //5 ShowCmd  SW_SHOW, SW_SHOWNORMAL, SW_SHOWMAXIMIZED, SW_SHOWMINIMIZED, SW_SHOWMINNOACTIVE, SW_HIDE
        ComponentNumber := ComponentNumber+1;
        IDDictionary.Append(ConfigStringValues[14]); 
      end;
    end;
  end;
  except
  //MsgBox('.', mbInformation, mb_Ok);
  finally
  end;

  NoShortcutsCheckBox := TNewCheckBox.Create(SettingsPage);
  NoShortcutsCheckBox.Parent := SettingsPage.Surface;
  NoShortcutsCheckBox.Top := ComponentsCheckListBox.Top + ComponentsCheckListBox.Height + 8;
  NoShortcutsCheckBox.Left := ComponentsCheckListBox.Left + 245;
  NoShortcutsCheckBox.Width := ComponentsCheckListBox.Width - 200;
  NoShortcutsCheckBox.Caption := 'Do not create optional shortcuts';  
end;

procedure CreateReadyPage;
begin
   ReadyPage := CreateOutputMsgMemoPage(SettingsPageID, 'Ready to Install', 'Setup is now ready to begin installing ' + '{#appName}' + ' on your computer.','Click Next to continue with the installation, or click Back if you want to review or change any settings. ','');
   ReadyPage.RichEditViewer.UseRichEdit := True;
   ReadyPage.RichEditViewer.ScrollBars := ssVertical;
end;

procedure UpdateReadyPage;
var
  ComponentFullPath: String;
  ComponentList: String;
  index: Integer;
begin 
  ComponentList := 'Selected components:' + CRLF;
  for index := 0 to (ComponentNumber - 1) do
  begin
     ComponentFullPath := GetCheckedComponentFullPath(ComponentsCheckListBox, index);
     if ComponentFullPath <> '' then
     begin
       ComponentList := ComponentList + CRLF + '     ' + GetComponentFullName(ComponentsCheckListBox, index);
    end;
  ReadyPage.RichEditViewer.RTFText := ComponentList;
  end;
end;

procedure CreateInstallationProgressPage;
begin
   InstallationProgressPage := CreateOutputProgressPage('Installation in progress', '');
end;

function RemoveComponentParameter(var ComponentParams: String; const ParameterToRemove: String): Integer;
begin
	Result := StringChangeEx(ComponentParams, ParameterToRemove, '', True);
end; 

function NextButtonClick(CurPageID: Integer): Boolean;
var
  ResultCode: Integer;
  ComponentFullPaths: TStringList;
  ComponentNames: TStringList;
  NumComponents: Integer;
  ProgressStep: Integer;
  i: Integer;
  ComponentIndex: Integer;
  ComponentIndexes: TStringList;
  ComponentParams: String;
  ComponentShowCmd: Integer;
                                                                                                    
begin
  Result := True;

  case CurPageID of
  
  SettingsPageID: UpdateReadyPage;
  
  ReadyPageID:
  begin
    
    NumComponents := GetNumCheckedComponents(ComponentsCheckListBox);
    FileCopy(({#InstallSetupINI}), ({#UninstallSetupINI}), False);
    if NumComponents<>0 then
    begin
      InstallationProgressPage.Show;
      ComponentNames := GetCheckedComponentNames(ComponentsCheckListBox);
      ComponentFullPaths := GetCheckedComponentPaths(ComponentsCheckListBox);
      ComponentIndexes := GetCheckedComponentIndexes(ComponentsCheckListBox);

      ProgressStep := 100 div NumComponents;
    
      for i := 0 to (NumComponents - 1) do
      begin
        ComponentIndex := StrToInt(ComponentIndexes[i]);
        ComponentParams := GetComponentParams(ComponentsCheckListBox, ComponentIndex);
        
        if (not(NoShortcutsCheckBox.Checked) and (ComponentParams<>'')) then RemoveComponentParameter(ComponentParams,'/NOSHORTCUTS');
        
        ComponentShowCmd := GetComponentShowCmd(ComponentsCheckListBox, ComponentIndex);
        
        InstallationProgressPage.SetProgress(ProgressStep*i + (ProgressStep div 4), 100);
        InstallationProgressPage.SetText('Installing ' + ComponentNames.Strings[i] + '...', ComponentFullPaths.Strings[i]);
        //Sleep(2000);
   
        Exec(ComponentFullPaths.Strings[i], ComponentParams , '', ComponentShowCmd, ewWaitUntilTerminated, ResultCode);
        InstallationProgressPage.SetProgress(ProgressStep*(i+1), 100);
      end;
      
      InstallationProgressPage.SetProgress(100, 100);
      Sleep(1000);                               
      InstallationProgressPage.Hide;
    end;
   end;
  end;
 end;

procedure InitializeFinishedPage();
var
  FinishedLabelRestartWarning: TNewStaticText;

begin
  //see 'Custom Messages' section to modify 'FinishedLabelRestartWarning' message   
  FinishedLabelRestartWarning := TNewStaticText.Create(WizardForm);
  FinishedLabelRestartWarning.Parent := WizardForm.FinishedPage;
  FinishedLabelRestartWarning.Left := WizardForm.FinishedLabel.Left;
  FinishedLabelRestartWarning.Top := WizardForm.FinishedLabel.Top + WizardForm.FinishedLabel.Height + 50;
  FinishedLabelRestartWarning.Width := WizardForm.FinishedLabel.Width;
  FinishedLabelRestartWarning.Height := WizardForm.FinishedPage.Height - WizardForm.FinishedLabel.Top;
  FinishedLabelRestartWarning.WordWrap := True;
  FinishedLabelRestartWarning.Font.Style := [fsBold];
  FinishedLabelRestartWarning.Font.Color := clRed;
  FinishedLabelRestartWarning.Caption := CustomMessage('FinishedLabelRestartWarning');
end;

procedure InitializeWizard();
begin
      CreateSettingsPage;
      CreateReadyPage;
      CreateInstallationProgressPage;
      CreateURLLabel(WizardForm);
      InitializeFinishedPage;
end;

//===============Uninstall==================

procedure InitializeUninstallProgressForm();
begin
      CreateURLLabel(UninstallProgressForm);
end;

procedure CurUninstallStepChanged(CurUninstallStep: TUninstallStep);
var
      ConfigStrings: TArrayOfString;
      ConfigStringValues: TArrayOfString;
      ComponentUninstaller: String;
      ComponentUninstallers: TStringList;
      i: Integer;
      ResultCode: Integer;
      NumComponents: Integer;
      ProgressStep: Integer;
     begin
      case CurUninstallStep of
      usUninstall:
         begin
            ComponentUninstallers := TStringList.Create;
            LoadStringsFromFile(({#UninstallSetupINI}), ConfigStrings);
            for i := 0 to (GetArrayLength(ConfigStrings)-1) do
               begin
                 ConfigStringValues := StrSplit(ConfigStrings[i], ';'); 
                 ComponentUninstaller := ExpandConstant('{commonappdata}')+ '\{#uninstFolder}\' + ConfigStringValues[12];
                 if FileExists(ComponentUninstaller) then
                 begin
                      ComponentUninstallers.Append(ComponentUninstaller);
                 end;
               end;
            NumComponents := ComponentUninstallers.Count;
            if NumComponents > 0 then
            begin
                UninstallProgressForm.ProgressBar.Min := 0;
                UninstallProgressForm.ProgressBar.Max := 100;
                ProgressStep := 100 div NumComponents;
                for i := 0 to (NumComponents - 1) do
                begin
                   Exec(ComponentUninstallers[i], '/VERYSILENT', '', 1, ewWaitUntilTerminated, ResultCode);   
//                   UninStallProgressForm.StatusLabel.SetText(('Uninstalling ' + ComponentNames.Strings[i] + '...', ComponentFullPaths.Strings[i]));
                   UninstallProgressForm.ProgressBar.Position := ProgressStep*(i+1);
                end;
            end;
           DeleteFile({#UninstallSetupINI});
         end;
   end;
end;

function InitializeUninstall(): Boolean;
begin
     Result := True;
end;

//#expr SaveToFile(AddBackslash(SourcePath) + "preprocessed.iss")
