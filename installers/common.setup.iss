[Setup]      

#if xXX=="x64"
  ArchitecturesInstallIn64BitMode="x64"
  ArchitecturesAllowed="x64"
#else
  ArchitecturesAllowed="x86 x64"
#endif

AppId={{#AppId}
AppName="{#appName} {#packageVersionTextName}"
AppVersion="{#version3}.{#revision}{#prerelease}"
VersionInfoVersion={#version3}.{#revision}
UninstallDisplayName="{#appName} {#packageVersionTextName}"
VersionInfoProductName="{#appName}"
OutputDir="{#outputDir}\{#packageVersion}{#prerelease}{#dp}"
OutputBaseFilename="{#outputBaseFilename}"
WizardSmallImageFile="{#wizardSmallImageFile}"
WizardImageFile="{#wizardImageFile}"
UninstallFilesDir="{commonappdata}\{#uninstFolder}"
UninstallDisplayIcon="{commonappdata}\{#uninstFolder}\installer.ico"
DefaultDirName={#defaultDirName}

AppPublisher=NOFFZ Technologies GmbH
AppPublisherURL=http://www.noffz.com
AppContact=info@noffz.com
AppSupportPhone=+49 2151-99878-0
Uninstallable=yes
WizardImageStretch=False
VersionInfoCompany=NOFFZ Technologies GmbH
DisableProgramGroupPage=yes
RestartIfNeededByRun=False
ShowTasksTreeLines=True
AppCopyright=NOFFZ Technologies GmbH
EnableDirDoesntExistWarning=True
DirExistsWarning=no
ShowLanguageDialog=no
UsePreviousAppDir=False

;CloseApplication=force will close applications when updating the files
CloseApplications=yes
ChangesAssociations=yes
CloseApplicationsFilter = *.*


#expr SaveToFile(AddBackslash(SourcePath) + "preprocessed.iss")